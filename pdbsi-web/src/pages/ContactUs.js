import { Container, Component, Col, Form, Row, Button  } from 'react-bootstrap';

// For sweet alert
import Swal from 'sweetalert2';

import React, { useState, useEffect, useRef } from 'react';

// For EmailJS
import emailjs from '@emailjs/browser';

// For IP Geolocation
import axios from 'axios';

// For Wysiwyg Text Editor 
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'
import { Editor } from 'react-draft-wysiwyg';

import  ReactQuill  from  "react-quill";
import  "react-quill/dist/quill.snow.css";



// For Mui
import { styled } from '@mui/material/styles';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';




	export default function ContactUs() {
	
	
  // Getting and Setting values for each text fields
  const [getName, setName] = useState("");
  const [getEmail, setEmail] = useState("");
  const [getConcern, setConcern] = useState("")
  const [convertedText, setConvertedText] = useState("");

  // For validation
  const [getValidName, setValidName] = useState(true);
  const [getValidEmail, setValidEmail] = useState(true);
  const [getValidConcern, setValidConcern] = useState(true)
  const [getValidInquiry, setValidInquiry] = useState(true)
  const [validConvertedText, setValidConvertedText] = useState(true);

  // For Geolocation
  const [ipAddress, setIpAddress] = useState("")
  const [country, setCountry] = useState("")
  const [city, setCity] = useState("")
  var url = "https://ipgeolocation.abstractapi.com/v1/?api_key=541a7c8e44104a8191dacfaf28ce6d04"

  // For EmailJs
  const form = useRef();
  

  // Validation
  const validate = () => {
		const arr = [getEmail, getConcern, getName, convertedText];
		const error = [];
	 arr.forEach(function (task) {
	    if(task !== false){
	    error.push(task); 
	    }
	});
		 emptyfields(error)
	}

	const emptyfields = (error) => {
		for(let i = 0; i < error.length; i++){
			console.log(error)
			
			// Empty Fields 
			if(0 === i && error[i] === ""){
				setValidEmail(false)

			} else if (1 === i && error[i] === "") {
				setValidConcern(false)

			} else if(2 === i && error[i] === "") {
				setValidName(false)

			} else if (3 === i && error[i] === "") {

				setValidConvertedText(false)
			} 

			// If fields has a value
				else if (0 === i && error[i] !== "") {
				setValidConcern(true)

			}
				else if (1 === i && error[i] !== "") {
				setValidConcern(true)

			} else if(2 === i && error[i] !== "") {
				setValidName(true)
 
			} else if (3 === i && error[i] !== "") {

				setValidConvertedText(true)
			}
		}
	}

function submitEmail(e) {
    e.preventDefault()
    if(getEmail !== "" && convertedText !== "" && getConcern !== "" && getName !== ""){
 			// YOUR_SERVICE_ID, YOUR_TEMPLATE_ID, YOUR_PUBLIC_KEY
 				// Send User message to our Email
    	emailjs.sendForm('service_oase9qr', 'template_e4kl58l', form.current, 'afnpFQF89AtBjo2Eo')
      .then((result) => {
      		//  Send Auto Reply
      	emailjs.sendForm('service_oase9qr', 'template_51rzmtd', form.current, 'afnpFQF89AtBjo2Eo')
      .then((data) => { 

      						// Save to Database
      	        fetch(`${window.server}/users/register`, {
						    		method: 'POST',
						    		headers: {
						    			"Content-Type": "application/json"
						    		},
						    		body: JSON.stringify({
										email: getEmail,
										message: convertedText,
										concern: getConcern,
										ipAddress: ipAddress,
										country: country,
										city: city
									})
								})
								.then(res => res.json())
								    .then(data => {

								    	// handleClose()	
								    	const Toast = Swal.mixin({
										  toast: true,
										  position: 'bottom-end',
										  showConfirmButton: false,
										  timer: 3000,
										  timerProgressBar: false,
										  didOpen: (toast) => {
										    toast.addEventListener('mouseenter', Swal.stopTimer)
										    toast.addEventListener('mouseleave', Swal.resumeTimer)
										  }
										})
								   Toast.fire({
										  icon: 'success',
										  title: 'Email Submitted!'
										})		
						    	})
								    // Reset Values
								    setEmail("")
								    setConcern("")
								    setName("")
								    setConvertedText("")
								    setValidEmail(true)
								    setValidConcern(true)
										setValidName(true) 
										setValidConvertedText(true)



       }, (error) => {
          console.log(error.text);
          const Toast = Swal.mixin({
				  toast: true,
				  position: 'bottom-end',
				  showConfirmButton: false,
				  timer: 3000,
				  timerProgressBar: false,
				  didOpen: (toast) => {
				    toast.addEventListener('mouseenter', Swal.stopTimer)
				    toast.addEventListener('mouseleave', Swal.resumeTimer)
				  }
				})
          				Toast.fire({
				  icon: 'error',
				  title: 'Error occured'
				})
      });	

      }, (error) => {
          console.log(error.text);
          const Toast = Swal.mixin({
				  toast: true,
				  position: 'bottom-end',
				  showConfirmButton: false,
				  timer: 3000,
				  timerProgressBar: false,
				  didOpen: (toast) => {
				    toast.addEventListener('mouseenter', Swal.stopTimer)
				    toast.addEventListener('mouseleave', Swal.resumeTimer)
				  }
				})
          				Toast.fire({
				  icon: 'error',
				  title: 'Please check your connection'
				})
      });	

    } else {
    	const Toast = Swal.mixin({
				  toast: true,
				  position: 'bottom-end',
				  showConfirmButton: false,
				  timer: 3000,
				  timerProgressBar: false,
				  didOpen: (toast) => {
				    toast.addEventListener('mouseenter', Swal.stopTimer)
				    toast.addEventListener('mouseleave', Swal.resumeTimer)
				  }
				})

				Toast.fire({
				  icon: 'error',
				  title: 'Please check fields'
				})
				validate()
    } 
   }

   // Contents of Toolbar
   const  modules  = {
    toolbar: [
        ["bold", "italic", "underline"],
        [{ list:  "ordered" }, { list:  "bullet" }],
        [{ align: "right" }, { align: "center" }, { align: "left" } ],
    ]
};

 useEffect(() => {
 	  // For IP Geolocation
 	  var xmlHttp = new XMLHttpRequest();
    xmlHttp.responseType = 'json';
    var data = xmlHttp.response
		xmlHttp.onreadystatechange = function(data) {
        if (xmlHttp.readyState === 4 && xmlHttp.status === 200){
        setIpAddress(xmlHttp.response.ip_address);
				setCountry(xmlHttp.response.country);
				setCity(xmlHttp.response.city);
        }
        // This commnet is to show all data from xmlHttp
			// console.log(xmlHttp.response)			
    }	
        xmlHttp.open("GET", url, true); // true for asynchronous
    		xmlHttp.send(null);
},[])

	return (

		<div className=" pt-5">
		  <div id="contactUs" className="contact-us">
				<Container className="py-3" >
					<h2 className="text-center"><strong>CONTACT US:</strong></h2>
								<Form className="custom-padding" ref={form} onSubmit={(e) => submitEmail(e)}>
							      <Form.Group as={Row} className="mb-3" >
							        <Form.Label column md="3">
							          Name:
							        </Form.Label>
							        <Col md="9">
									          {

									          	getValidName == false ?
									          	<>
									          		<Form.Control 
											          style={{borderColor:"red",borderWidth: "2px",boxShadow: "none"}}   
											          type="text" 
											          placeholder="Enter Name"
											          value={getName}
											          name="user_name" 
											          onChange={e => {
								                 		 setName(e.target.value)
								                	  }}		           
								                	  />
								                <span style={{fontSize: "12px", color: "red", padding: "10px 20px"}}>* Please enter your name</span>	  
								               </>
								               :
								               	<Form.Control 
											          style={{borderColor:"#000000",borderWidth: "2px",boxShadow: "none"}}   
											          type="text" 
											          placeholder="Enter Name"
											          value={getName}
											          name="user_name" 
											          onChange={e => {
								                 		 setName(e.target.value)
								                	  }}		           
								               	/>

						                }
							        </Col>
							      </Form.Group>


							      <Form.Group as={Row} className="mb-3">
							        <Form.Label column sm="3">
							          Email Address:
							        </Form.Label>
									        <Col sm="9">
											        {

											        	getValidEmail == false ?
											        	<>
											        	<Form.Control 
											          style={{borderColor:"red",borderWidth: "2px",boxShadow: "none"}} 
											          type="email" 
											          placeholder="Enter Email Address"
											          value={getEmail}
											          name="user_email" 
											          onChange={e => {
								                 		 setEmail(e.target.value)
								                	  }} 
											          />
											          <span style={{fontSize: "12px", color: "red", padding: "10px 20px"}}>* Please enter your email</span>
											          </>
											          :
											          <Form.Control 
											          style={{borderColor:"#000000",borderWidth: "2px",boxShadow: "none"}} 
											          type="email" 
											          placeholder="Enter Email Address"
											          value={getEmail}
											          name="user_email" 
											          onChange={e => {
								                 		 setEmail(e.target.value)
								                	  }} 
											          />

											        }
									        </Col>
							      </Form.Group>


							      <Form.Group as={Row} className="mb-3">
							        <Form.Label column sm="3">
							          Concern:
							        </Form.Label>
											     <Col sm="9">
											         {

													         getValidConcern == false ?
													        <> 
													        <Form.Select 
													          style={{borderColor:"red",borderWidth: "2px",boxShadow: "none"}}  
													          aria-label="Default select example"
													          onChange={e => {
										                 		 setConcern(e.target.value)
										                	  }}
										                placeholder="Option"
													          >
															      <option>Select</option>
															      <option value="Web Development">Web Development</option>
															      <option value="Video and Photo Production">Video and Photo Production</option>
															      <option value="Social Media Management">Social Media Management</option>
															      <option value="Community Management<">Community Management</option>
														   		</Form.Select>
														   		<span style={{fontSize: "12px", color: "red", padding: "10px 20px"}}>* Please select your subject</span>
														   		</>
														    :
														    	<Form.Select 
													          style={{borderColor:"#000000",borderWidth: "2px",boxShadow: "none"}}  
													          aria-label="Default select example"
													          name="concern"
													          onChange={e => {
										                 		 setConcern(e.target.value)
										                	  }}
										                placeholder="Option"
													          >
														        <option>Select</option> 
															      <option value="Web Development">Web Development</option>
															      <option value="Video and Photo Production">Video and Photo Production</option>
															      <option value="Social Media Management">Social Media Management</option>
															      <option value="Community Management<">Community Management</option>
														   	 </Form.Select>

												  		}
											   </Col>
							      </Form.Group>


							      <Form.Group  as={Row} className="mb-3">
							        <Form.Label column sm="3">
							          Inquiry:
							        </Form.Label>
									        <Col sm="9">
																{	

																	validConvertedText == false ?
																	<div>
															      <ReactQuill
															        modules={modules}
															        theme='snow'
															        value={convertedText}
															        onChange={setConvertedText}
															        style={{  minHieght : "200px",height:  "200px", width: "100%", display: "flex", flexDirection: "column-reverse", border: "2px solid red", borderRadius: "8px", overflow: "hidden", background: "white"}}
															        placeholder="How can we help you?"
															      />
															      <span style={{fontSize: "12px", color: "red", padding: "10px 20px"}}>* Please enter your message</span>
																	</div>
																	:
																	<div>
															      <ReactQuill
															        modules={modules}
															        theme='snow'
															        value={convertedText}
															        onChange={setConvertedText}
															        style={{  minHieght : "200px",height:  "200px", width: "100%", display: "flex", flexDirection: "column-reverse", border: "2px solid", borderRadius: "8px", overflow: "hidden", background: "white"}}
															        placeholder="How can we help you?"
															      />
																	</div>

																}
									        </Col>
							      </Form.Group>

							      <div className="text-center" >
									      <Button type="submit" value="Send" style={{border: "none",boxShadow: "none", fontFamily: "Inter", whiteSpace: "nowrap"}}
									        className="SubmitButton m-2">SUBMIT</Button>
							      </div>
					    </Form>
					</Container>
				</div>
		</div>
		)
}
